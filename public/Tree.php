<?php


class Tree
{
    private $elements = [];
    private $childs = [];
    private $root;
    private $tree;

    public function add($id, $parentId, $value)
    {
        if($parentId == null) {
            $this->root = $id;
        }
        else {
            if(array_key_exists($parentId, $this->childs)) {
                array_push($this->childs[$parentId], $id);
            } else {
                $this->childs[$parentId] = [$id];
            }
        }
        $this->elements[$id] = $value;
    }


    private function render()
    {
        $this->build($this->root);
    }


    private function build($id, $level = 0)
    {
        $this->tree .= str_repeat('____', $level) . ' ' . $this->elements[$id] . "<br>";

        if(isset($this->childs[$id])) {
            foreach ($this->childs[$id] as $child) {
                $this->build($child, $level + 1);
            }
        }
        else {
            return;
        }
    }


    public function view()
    {
        $this->render();
        echo $this->tree;
    }

}



class Node
{
    public $id;
    public $parentId;
    public $value;
    public $child = [];

    public function __construct($id, $parentId, $value)
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->value = $value;
    }

}

